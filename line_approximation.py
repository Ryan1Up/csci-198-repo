import matplotlib
from matplotlib import pyplot as plt
import numpy as np
import random

X = np.array([1, 1.2, 2, 3, 3.4, 4, 4.5])
Y = np.array([2,   3, 3, 4,   3, 5, 4.5])


# m = random.randint(0, 5) 
# c = random.randint(0, 5) 
plt.scatter(X, Y)

m = 1
c = 4
def f(x):
    global m, c
    return m * x + c

plt.plot(np.linspace(0.0, 6.0, 10), f(np.linspace(0.0, 6.0, 10)))

a = 0.01
epoch = 5000
adapt = 40
std_dev = np.std(Y)
n = float(len(X))
for i in range(epoch):
    exp = f(X)
    plt.plot(X, exp)
    diff = Y - exp
    diff_sq = diff**2
    # if adapt is None:
    #     adapt = np.mean(np.mean(diff_sq) + np.max(diff_sq))
    #     # print(adapt)
    # else:
    #     avg_error = np.mean(np.mean(diff_sq) + np.max(diff_sq))
    #     if(avg_error < std_dev):
    #         adapt = std_dev
    #     else:
    #         adapt = avg_error
    step = sum(diff * (1./(np.cosh((diff_sq/adapt))**2)))
    d_m = ((-2.*m)/n) * step
    d_c = (-2./n)     * step

    m -= a * d_m
    c -= a * d_c


# for i in range(epoch):
#     pred = m*X + c
#     dm = (-2/n) * sum(X* (Y - pred))
#     dc = (-2/n) * sum((Y - pred))
#     m = m - a*dm #update m with learning rate times derivative
#     c = c - a*dc


plt.plot(np.linspace(0.0, 6.0, 10), f(np.linspace(0.0, 6.0, 10)))
plt.xlim([0, 6])
plt.ylim([0, 6])
plt.show()